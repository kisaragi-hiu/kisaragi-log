;; -*- mode: lisp-interaction; -*-
(when (require 'undercover nil t)
  (undercover "*.el"))

(require 'buttercup)
(require 'kisaragi-log)
(require 'kisaragi-log-data)
(require 'kisaragi-log-vars)

(describe "Date stuff"
  (it "parses ISO dates well enough"
    (expect
     ;; Both of these use the current timezone on the system.
     (kisaragi-log--parse-date "2020-02-25")
     :to-equal
     (encode-time 0 0 0 25 2 2020))
    (expect
     (kisaragi-log--parse-date "2018-08-32")
     :to-equal
     (encode-time 0 0 0 32 8 2018)))
  (it "Figures out today"
    (expect
     ;; I'll skip on testing handling o-e-t-u properly.
     (let ((org-extend-today-until 0))
       (kisaragi-log--today))
     :to-equal
     (format-time-string "%F")))
  (it "Figures out the next day from a day"
    (expect
     (kisaragi-log--day-next "2020-01-01" 1)
     :to-equal "2020-01-02")
    (expect
     (kisaragi-log--day-next "2020-01-31" 1)
     :to-equal "2020-02-01")
    (expect
     (kisaragi-log--day-next "2020-02-28" 1)
     :to-equal "2020-02-29")
    (expect
     (kisaragi-log--day-next "2021-02-28" 1)
     :to-equal "2021-03-01")
    (expect
     (kisaragi-log--day-next "2021-02-28" 3)
     :to-equal "2021-03-03"))
  (it "returns list of dates between two dates"
    (expect
     (kisaragi-log--dates-between "2020-07-15" "2020-07-31")
     :to-equal
     '("2020-07-31" "2020-07-30" "2020-07-29" "2020-07-28" "2020-07-27"
       "2020-07-26" "2020-07-25" "2020-07-24" "2020-07-23" "2020-07-22"
       "2020-07-21" "2020-07-20" "2020-07-19" "2020-07-18" "2020-07-17"
       "2020-07-16" "2020-07-15"))))

(describe "Target visualizer"
  (it "displays whether target is passed"
    (expect
     (kisaragi-log--target-visualizer 150 20)
     :to-equal "✓"))
  (it "shows passed with an amount equal to target"
    (expect
     (kisaragi-log--target-visualizer 1500 1500)
     :to-equal "✓"))
  (it "always shows an x when amount is 0"
    (expect
     (kisaragi-log--target-visualizer 0 0)
     :to-equal "×")
    (expect
     (kisaragi-log--target-visualizer 0 1000)
     :to-equal "×")))

(describe "Portion visualizer"
  (it "creates a portion visualizer"
    (expect
     (kisaragi-log--portion-visualizer 400 1000 10)
     :to-equal
     "|####------|")
    (expect
     (kisaragi-log--portion-visualizer 800 1000 30)
     :to-equal
     "|########################------|")))

(describe "data"
  (let (kisaragi-log-data-file)
    (before-all
      (setq kisaragi-log-data-file (make-temp-file "kisaragi-log")))
    (after-all
      (delete-file kisaragi-log-data-file))
    (it "writes and reads"
      (expect
       (progn (kisaragi-log-data-write nil)
              (kisaragi-log-data-read))
       :to-be nil)
      (expect
       (progn (kisaragi-log-data-write "abc")
              (kisaragi-log-data-read))
       :to-equal "abc")
      (expect
       (progn (kisaragi-log-data-write '("a" "b"))
              (kisaragi-log-data-read))
       :to-equal ["a" "b"])
      (expect
       (progn (kisaragi-log-data-write
               #s(hash-table size 65
                             test equal
                             rehash-size 1.5
                             rehash-threshold 0.8125
                             data ("2020-01-01" 100 "2021-01-01" 200)))
              (kisaragi-log-data-read))
       :to-have-same-items-as '((2020-01-01 . 100) (2021-01-01 . 200))))
    (it "updates with a function"
      (expect
       (progn (kisaragi-log-data-write '((a . "b") (c . "e")))
              (kisaragi-log-data-update
               (lambda (data)
                 (push '(f . "g") data)
                 data))
              (kisaragi-log-data-read))
       :to-have-same-items-as '((a . "b") (c . "e") (f . "g"))))
    (it "reads with a function"
      (expect
       (progn (kisaragi-log-data-write '((a . "b") (c . "e")))
              (kisaragi-log-data-do
               (lambda (data)
                 (alist-get 'a data))))
       :to-equal "b"))))
