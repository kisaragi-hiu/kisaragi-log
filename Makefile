.cask: Cask
	cask install

test: $(shell find . -name "*.el") .cask
	cask exec buttercup -L . -L tests

.PHONY: test
