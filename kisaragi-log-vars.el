;;; kisaragi-log-vars.el --- Global variables  -*- lexical-binding: t; -*-

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defgroup kisaragi-log nil
  "Logging amounts of stuff."
  :group 'convenience
  :prefix "kisaragi-log-")

(defcustom kisaragi-log-data-file (expand-file-name "log.json" user-emacs-directory)
  "File to store log data in."
  :group 'kisaragi-log
  :type 'file)

(defcustom kisaragi-log-types
  '(("water" ((:amounts . (350 600))
              (:target . 1500))))
  "Types of stuff to log.

Each type is a list that looks like:

  (type-name options)

Where OPTIONS is a map (plist, alist, or hash table). The keys are
always keywords (`:amounts', not `amounts').

AMOUNTS is a list of preset amounts shown in the prompt.
TARGET is the daily target.

Full example:

  (type-name (:amounts AMOUNTS
              :target TARGET))"
  :group 'kisaragi-log
  :type
  '(repeat
    (string :tag "Type")
    ;; giving up.
    (sexp :tag "Options")))

(defcustom kisaragi-log-first-day nil
  "Don't show entries before this day.

If set, this should be a string in the format \"YYYY-MM-DD\"."
  :group 'kisaragi-log
  :type '(choice
          (string :tag "Date (YYYY-MM-DD)")
          (const :tag "Earliest day logged" nil)))

(defvar kisaragi-log--amount-history nil "History for `kisaragi-log--read-amount'.")

(defconst kisaragi-log-buffer-name "*kisaragi-log: %s*"
  "Buffer name for the `kisaragi-log-visualize' buffer.

Contains a %s which will be replaced with the type.")

(defvar-local kisaragi-log-visualize--type nil
  "Local variable to tell view commands which type we're visualizing.")

(provide 'kisaragi-log-vars)
;;; kisaragi-log-vars.el ends here
