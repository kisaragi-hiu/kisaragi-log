;;; kisaragi-log-data.el --- Data storage and retrieval  -*- lexical-binding: t; -*-

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Storing data into a JSON.

;; JSON is structured like:

;; {"<date>": {"type1": <amount>, "type2": <amount>},
;;  "<date>": {"type1": <amount>, "type2": <amount>},
;;  ...}

;; If a date or type isn't found, return 0 for the amount.

;;; Code:

(require 'json)
(require 'cl-lib)

(require 'kisaragi-log-vars)

;;;; Reading and writing the file
(defun kisaragi-log-data-write (data)
  "Encode DATA as json and write it into log file."
  (with-temp-file kisaragi-log-data-file
    (let ((json-encoding-pretty-print t)
          (json-encoding-object-sort-predicate #'string>))
      (insert (json-encode data)))))

;; Putting this in its own function allows us to maybe change the way
;; we store the data, if JSON turns out to be too slow.
(defun kisaragi-log-data-read ()
  "Read data from `kisaragi-log-data-file'.

If the file doesn't exist, return an empty list."
  (if (file-exists-p kisaragi-log-data-file)
      (json-read-file kisaragi-log-data-file)
    ;; This allows `kisaragi-log-data-update' to just write "{}"
    ;; if the file doesn't already exist.
    '()))

(defun kisaragi-log-data-update (func)
  "Use FUNC to update log data.

FUNC receives one argument, the parsed log data; its return
value is encoded back into JSON then written back out."
  (thread-last (kisaragi-log-data-read)
    (funcall func)
    kisaragi-log-data-write))

(defun kisaragi-log-data-do (func)
  "Read log data and pass it to FUNC."
  (funcall func (kisaragi-log-data-read)))

(defun kisaragi-log-get (data day type)
  "Get amount logged for TYPE on DAY.

DATA is the parsed JSON.

If no log was found for TYPE on DAY, return 0.

DAY should be a string in the format \"YYYY-MM-DD\"."
  (cl-assert (stringp day))
  (cl-assert (stringp type))
  (alist-get (intern type)
             (alist-get (intern day)
                        data)
             0))
(gv-define-setter kisaragi-log-get (new data day type)
  `(setf (alist-get (intern ,type)
                    (alist-get (intern ,day)
                               ,data))
         ,new))

(provide 'kisaragi-log-data)

;;; kisaragi-log-data.el ends here
