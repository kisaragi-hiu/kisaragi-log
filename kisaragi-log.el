;;; kisaragi-log.el --- Tracking stuff like water intake -*- lexical-binding: t; -*-

;; Authors: Kisaragi Hiu <mail@kisaragi-hiu.com>
;; URL: https://kisaragi-hiu.com/projects/kisaragi-log
;; Version: 3.1.0
;; Package-Requires: ((emacs "25.1") (transient "0.2.0"))
;; Keywords: convenience productivity

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program. If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Generic log of stuff

;; - Data stored in JSON
;; - eg. Track how much water I drank with kisaragi-log-commit
;; - A visualizer

;;; Code:

(require 'subr-x)
(require 'cl-lib)
(require 'json)
(require 'transient)
(require 'map)

;; For `color-clamp': added in Emacs 24, commit
;; 6725d21a1be13cfad897dab54509928c3f5b5d1e
(require 'color)

(require 'kisaragi-log-data)
(require 'kisaragi-log-vars)

;;;; Other utils

(defun kisaragi-log--parse-date (date)
  "Parse DATE into an encoded time value.

DATE should be a string in the format \"YYYY-MM-DD\". An actually
usable ISO date parser should be in a dedicated library, not as
part of Org."
  ;; Mostly taken from `ledger-parse-iso-date', except the regexp is
  ;; greatly simplified because we're really only ever reading dates
  ;; generated by `kisaragi-log--today'.
  (save-match-data
    (when (string-match "\\(....\\)-\\(..\\)-\\(..\\)" date)
      (encode-time 0 0 0
                   (string-to-number (match-string 3 date))
                   (string-to-number (match-string 2 date))
                   (string-to-number (match-string 1 date))))))

(defun kisaragi-log--today ()
  "Return today's date, taking `org-extend-today-until' into account.

Return values look like \"2020-01-23\"."
  (format-time-string
   "%Y-%m-%d"
   (time-since
    ;; if it's bound and it's a number, do the same thing `org-today' does
    (or (and (boundp 'org-extend-today-until)
             (numberp org-extend-today-until)
             (* 3600 org-extend-today-until))
        ;; otherwise just return (now - 0) = now.
        0))))

(defun kisaragi-log--day-next (day n)
  "Return N days after DAY, as a date stamp."
  (thread-last (kisaragi-log--parse-date day)
    (time-add (* 86400 n))
    (format-time-string "%Y-%m-%d")))

(defun kisaragi-log--dates-between (start end)
  "Return a list of dates between START and END, including both.

START and END should be strings in the format \"YYYY-MM-DD\".

START must be earlier than END."
  (let ((start (kisaragi-log--parse-date start))
        (end (kisaragi-log--parse-date end)))
    (let* ((i start)
           (result nil))
      (while (not (time-less-p end i))
        (push (format-time-string "%F" i) result)
        (setq i (time-add 86400 i)))
      result)))

(defun kisaragi-log--read-type (&optional prompt)
  "Read a type from the user.

Use PROMPT as the prompt."
  (let ((types (map-keys kisaragi-log-types)))
    (if (= (length types) 1)
        (progn
          (message "Selecting \"%s\", the only declared type" (car types))
          (car types))
      (completing-read (or prompt "Type: ")
                       (mapcar #'car kisaragi-log-types)))))

(defun kisaragi-log--read-amount ()
  "Read an amount of some stuff from the user.

Prompt for which type of stuff and for how much the user wants to
log.

Return (type amount)."
  (let* ((type (kisaragi-log--read-type))
         (preset-amounts (kisaragi-log-type-get type :amounts))
         (amount
          (completing-read "Amount: "
                           (mapcar #'number-to-string preset-amounts)
                           nil nil nil
                           'kisaragi-log--amount-history)))
    (list type (string-to-number amount))))

;; Canrylog-view needs to be its own library
(defmacro kisaragi-log--view-update (&rest body)
  "Run BODY with boilerplate to update views."
  `(let ((inhibit-read-only t)
         (line (line-number-at-pos))
         (column (current-column)))
     (erase-buffer)
     (prog1 (progn ,@body)
       ;; restore point after updating buffer
       (setf (point) (point-min))
       (ignore-errors
         (forward-line (1- line))
         (forward-char (1- column))
         (recenter)))))

(defun kisaragi-log--target-visualizer (amount target)
  "Return \"×\" if AMOUNT is smaller than or equal to TARGET, \"✓\" otherwise."
  (let ((no "×")
        (yes "✓"))
    ;; As we don't know if target is the real target or just the
    ;; amount from yesterday, special casing 0 is the better option.
    (if (or (= amount 0)
            (< amount target))
        no
      (add-face-text-property 0 1 'org-todo nil yes)
      yes)))

(defun kisaragi-log--portion-visualizer (amount max width)
  "Return a portion visualizer.

Example with WIDTH = 10 (notice how there are 10 dashes):

  |----------|

AMOUNT divided by MAX determines how filled the visualizer is.
For example, with WIDTH = 10, AMOUNT = 400, MAX = 1000, this returns:

  |####------|"
  (let* ((filled (floor (* width (color-clamp (/ amount max 1.0)))))
         (empty (- width filled)))
    (concat
     "|"
     (make-string filled ?#)
     (make-string empty ?-)
     "|")))

(defun kisaragi-log--first-day ()
  "Return first day to start logging about."
  (or kisaragi-log-first-day
      (kisaragi-log-data-do
       (lambda (data)
         (thread-first (map-keys data)
           ;; FIXME: relies on data being already sorted
           last
           car
           symbol-name)))))

;;;; Log type config

(defun kisaragi-log-type-get (type key)
  "Get the config value for KEY in TYPE."
  (map-elt (cadr (assoc type kisaragi-log-types)) key))

;;;; Main commands

;;;###autoload
(cl-defun kisaragi-log-commit (type amount &optional (date (kisaragi-log--today)))
  "Commit AMOUNT into data for TYPE on DATE.

DATE defaults to today, honoring `org-extend-today-until' if
available. It should be given as a string in YYYY-MM-DD."
  (interactive (kisaragi-log--read-amount))
  (kisaragi-log-data-update
   (lambda (data)
     (cl-incf
      (alist-get (intern type)
                 (alist-get (intern date)
                            data)
                 0)
      amount)
     data)))

(defvar kisaragi-log-visualize-mode-map
  (let ((map (make-sparse-keymap)))
    map)
  "Keymap in the `kisaragi-log-visualize' buffer.")

(define-derived-mode kisaragi-log-visualize-mode special-mode
  "Kisaragi-Log-Visualize" "Major mode for kisaragi-log-visualize view."
  :group 'kisaragi-log
  (setq-local revert-buffer-function #'kisaragi-log-visualize--revert))

;;;###autoload
(defun kisaragi-log-visualize (type)
  "Visualize log data for TYPE."
  (interactive (list (kisaragi-log--read-type)))
  (let ((inhibit-read-only t)
        (buffer (format kisaragi-log-buffer-name type)))
    (with-current-buffer (get-buffer-create buffer)
      (kisaragi-log-visualize-mode)
      (setq kisaragi-log-visualize--type type)
      (revert-buffer)
      (setf (point) (point-min)))
    (pop-to-buffer buffer)))

(defun kisaragi-log-visualize--revert (&rest _)
  "Actually render the kisaragi-log visualizer."
  (let* ((type kisaragi-log-visualize--type)
         (target (kisaragi-log-type-get type :target)))
    (kisaragi-log--view-update
     (insert (capitalize (format "%s log:\n\n" type)))
     (insert-text-button
      "[Change log type]"
      'action
      (lambda (&rest _)
        (setq kisaragi-log-visualize--type
              (kisaragi-log--read-type "Change to type: "))
        (revert-buffer)))
     (insert "\n\n")
     (kisaragi-log-data-do
      (lambda (data)
        (dolist (day (kisaragi-log--dates-between
                      (kisaragi-log--first-day)
                      (kisaragi-log--today)))
          (let ((amount (kisaragi-log-get data day type)))
            (unless (and (kisaragi-log-type-get type :skip-zero)
                         (= 0 amount))
              (insert
               (format "%s: %s %s %s\n"
                       day
                       (kisaragi-log--portion-visualizer
                        amount target 10)
                       (kisaragi-log--target-visualizer
                        amount
                        ;; Give a check if it's more than yesterday
                        (min (kisaragi-log-get
                              data
                              (kisaragi-log--day-next day -1)
                              type)
                             target))
                       amount))))))))))

;; Normally when creating the autoload file, functions that define
;; other commands would trigger an (autoload ...) statement to be
;; written to the autoload file. But this is determined with a
;; (def...) prefix, so it works with eg. defmacro or
;; define-transient-command but not transient-define-prefix or
;; evil-define-operator.
;;
;; To work around this we have to write the autoload statement
;; ourselves.

;;;###autoload (autoload 'kisaragi-log "kisaragi-log" "Invoke a `kisaragi-log' command." t nil)
(transient-define-prefix kisaragi-log ()
  "Invoke a `kisaragi-log' command."
  ["Add"
   [("h" "Commit an amount" kisaragi-log-commit)]]
  ["View"
   [("l" "View log" kisaragi-log-visualize)]])

(provide 'kisaragi-log)
;;; kisaragi-log.el ends here
